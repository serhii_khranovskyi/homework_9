package com.epam.springcloud.notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

@RequestMapping
@RestController
public class NotificationController {
    //private final Set<Notification> notifications = new HashSet<>(); <-- здесь нужно сохранять вместо Репозитории

    @Autowired
    NotificationRepository repository;

    @PostMapping("/notifications/{user}")
    public Notification notify(@PathVariable String user) {
        var notifier = new Random().nextInt(Notification.Notifier.values().length);
        repository.add(user, Notification.Notifier.values()[notifier]);
        return new Notification(user, Notification.Notifier.values()[notifier]);
    }

    @GetMapping("/")
    public List<Notification> getNotifications() {
        return repository.getAll();
    }
}
