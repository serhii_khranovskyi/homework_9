package com.epam.springcloud.notification;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Notification {
    String user;
    Notifier notifyBy = Notifier.EMAIL;

    enum Notifier {
        EMAIL,
        SMS,
        MMS,
        CALL
    }
}
