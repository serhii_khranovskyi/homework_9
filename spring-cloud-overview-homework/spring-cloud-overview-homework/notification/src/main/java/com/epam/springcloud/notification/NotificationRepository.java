package com.epam.springcloud.notification;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class NotificationRepository {

    private List<Notification> notificationList = new ArrayList<>();

    public void add(String name, Notification.Notifier notifier) {
        Notification notification = new Notification();
        notification.setUser(name);
        notification.setNotifyBy(notifier);
        notificationList.add(notification);
    }

    public List<Notification> getAll() {
        return notificationList;
    }
}
