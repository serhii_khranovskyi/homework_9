package com.pigorv.springcloud.orders;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "users", fallback = UserClient.UserClientImpl.class)
public interface UserClient {

    @GetMapping("/{userName}")
    String getUsersName(@PathVariable String userName);

    @Component
    class UserClientImpl implements UserClient{
        @Override
        public String getUsersName(String userName) {
            return "default user";
        }
    }
}
