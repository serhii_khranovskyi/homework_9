package com.pigorv.springcloud.orders;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "products", fallback = ProductClient.ProductClientImpl.class)
public interface ProductClient {
    @GetMapping("/{productName}")
    String getProductsName(@PathVariable String productName);

    @Component
    class ProductClientImpl implements ProductClient{
        @Override
        public String getProductsName(String productName) {
            return "thing";
        }
    }
}
